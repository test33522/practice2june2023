package com.amdocs.web.controller;
import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;

public class IncrementTest {

	@Test
	public void CounterCheck() {
	Increment incr =  new Increment();

	int result = incr.getCounter();
	assertEquals("Counter check",1,result);
}
	 @Test
        public void CounterDecrease() {
	Increment decr = new Increment();
        int result = decr.decreasecounter(1);
        assertEquals("Decrease check",1,result);
}

}
